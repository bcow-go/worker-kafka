package kafka

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"runtime/debug"
	"strings"
	"sync"
	"syscall"
	"time"

	"gopkg.in/confluentinc/confluent-kafka-go.v1/kafka"
)

type (
	Consumer       = kafka.Consumer
	ConfigMap      = kafka.ConfigMap
	Message        = kafka.Message
	TopicPartition = kafka.TopicPartition
	Offset         = kafka.Offset
	Error          = kafka.Error
	ErrorCode      = kafka.ErrorCode

	ErrorHandlerFunc func(err kafka.Error) (disposed bool)
)

type Worker struct {
	Topics           []string
	PollingTimeoutMs int
	PingTimeout      time.Duration
	ConfigMap        *ConfigMap
	ErrorHandler     ErrorHandlerFunc

	// kafka config
	bootstrapServers string // e.g: 127.0.0.1:9092,127.0.0.1:9093
	groupID          string

	messageHandlerFunc      MessageHandlerFunc
	unhandledMessageHandler UnhandledMessageHandler

	consumer         *Consumer
	consumerSyncOnce sync.Once
	wg               sync.WaitGroup
}

func (w *Worker) Start(ctx context.Context) {
	w.consumerSyncOnce.Do(func() {
		w.consumer = w.createConsumer()
	})

	workerCtx := &WorkerContext{
		processUnhandledMessage: w.processUnhandledMessage,
		consumer:                w.consumer,
	}

	defer func() {
		if e := recover(); e != nil {
			log.Printf("%+v\n%s", e, string(debug.Stack()))
		}
	}()

	c := w.consumer

	//register for interupt (Ctrl+C) and SIGTERM (docker)
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)

	log.Printf("[bcow-go/worker-kafka] %s listening topics [%s] on address %s\n",
		w.groupID,
		strings.Join(w.Topics, ","),
		w.bootstrapServers)

	err := c.SubscribeTopics(w.Topics, nil)
	if err != nil {
		panic(err)
	}

	for {
		select {
		case sig := <-sigchan:
			log.Printf("[bcow-go/worker-kafka] %% Caught signal %v: terminating\n", sig)
			return

		default:
			ev := c.Poll(0)
			if ev == nil {
				// hold up polling until got kafka.Event
				for {
					ev = c.Poll(w.PollingTimeoutMs)
					if ev != nil {
						break
					}
				}
			}

			switch e := ev.(type) {
			case *kafka.Message:
				w.processMessage(workerCtx, e)
			case kafka.Error:
				switch e.Code() {
				case kafka.ErrUnknownTopic, kafka.ErrUnknownTopicOrPart:
					if !w.handleKafkaError(e) {
						log.Fatalf("[bcow-go/worker-kafka] %% Error: (%#v) %+v: %v\n", e.Code(), e.Code(), e)
					}

				/* NOTE: https://github.com/edenhill/librdkafka/issues/64
				Currently the only error codes signaled through the error_cb are:
				- RD_KAFKA_RESP_ERR__ALL_BROKERS_DOWN - all brokers are down
				- RD_KAFKA_RESP_ERR__FAIL - generic low level errors (socket failures because of lacking ipv4/ipv6 support)
				- RD_KAFKA_RESP_ERR__RESOLVE - failure to resolve the broker address
				- RD_KAFKA_RESP_ERR__CRIT_SYS_RESOURCE - failed to create new thread
				- RD_KAFKA_RESP_ERR__FS - various file errors in the consumer offset management code
				- RD_KAFKA_RESP_ERR__TRANSPORT - failed to connect to single broker, or connection error for single broker
				- RD_KAFKA_RESP_ERR__BAD_MSG - received malformed packet from broker (version mismatch?)
				I guess you could treat all but .._TRANSPORT as fatal.
				*/
				case kafka.ErrTransport:
					if !w.handleKafkaError(e) {
						log.Printf("[bcow-go/worker-kafka] %% Error: (%#v) %+v: %v\n", e.Code(), e.Code(), e)
					}
					continue
				case kafka.ErrAllBrokersDown,
					kafka.ErrFail,
					kafka.ErrResolve,
					kafka.ErrCritSysResource,
					kafka.ErrFs,
					kafka.ErrBadMsg:
					log.Fatalf("[bcow-go/worker-kafka] %% Error: (%#v) %+v: %v\n", e.Code(), e.Code(), e)

				default:
					if !w.handleKafkaError(e) {
						log.Printf("[bcow-go/worker-kafka] %% Error: (%#v) %+v: %v\n", e.Code(), e.Code(), e)
					}
					return
				}
			default:
				log.Printf("[bcow-go/worker-kafka] %% Notice: Ignored %v\n", e)
				log.Printf("[bcow-go/worker-kafka] %% Notice: Ignored %#v\n", e)
			}
		}
	}
}

func (w *Worker) Stop(ctx context.Context) error {
	log.Printf("[bcow-go/worker-kafka] %% Stop\n")

	if w.consumer != nil {
		c := w.consumer
		c.Unassign()
		c.Unsubscribe()

		w.wg.Wait()

		return c.Close()
	}
	return nil
}

func (w *Worker) handleKafkaError(err kafka.Error) (disposed bool) {
	if w.ErrorHandler != nil {
		return w.ErrorHandler(err)
	}
	return false
}

func (w *Worker) processMessage(ctx *WorkerContext, message *kafka.Message) {
	w.wg.Add(1)
	defer func() {
		w.wg.Done()
	}()

	if w.messageHandlerFunc != nil {
		w.messageHandlerFunc(ctx, message)
	} else {
		w.unhandledMessageHandler.ProcessMessage(ctx, message)
	}
}

func (w *Worker) createConsumer() *Consumer {
	{
		v, _ := w.ConfigMap.Get(KAFKA_CONF_BOOTSTRAP_SERVERS, nil)
		if v == nil {
			panic(fmt.Sprintf("missing kafka config '%s'", KAFKA_CONF_BOOTSTRAP_SERVERS))
		}
		w.bootstrapServers = v.(string)
	}
	{
		v, _ := w.ConfigMap.Get(KAFKA_CONF_GROUP_ID, nil)
		if v == nil {
			panic(fmt.Sprintf("missing kafka config '%s'", KAFKA_CONF_GROUP_ID))
		}
		w.groupID = v.(string)
	}

	{
		brokerAddresses := strings.Split(w.bootstrapServers, ",")
		err := w.ping(brokerAddresses)
		if err != nil {
			log.Fatalf("[bcow-go/worker-kafka] %% Error: %+v\n", err)
		}
	}

	c, err := kafka.NewConsumer(w.ConfigMap)
	if err != nil {
		log.Fatalf("[bcow-go/worker-kafka] %% Error: %+v\n", err)
	}
	return c
}

func (w *Worker) processUnhandledMessage(ctx *WorkerContext, message *kafka.Message) {
	w.wg.Add(1)
	defer func() {
		w.wg.Done()
	}()

	w.unhandledMessageHandler.ProcessMessage(ctx, message)
}

func (w *Worker) ping(addresses []string) (err error) {
	timeout := w.PingTimeout
	for _, address := range addresses {
		var conn net.Conn
		conn, err = net.DialTimeout("tcp", address, timeout)
		if err != nil {
			continue
		}
		if conn != nil {
			defer conn.Close()
			log.Printf("[bcow-go/worker-kafka] tcp %s opened\n", address)
			return nil
		}
	}
	return
}
