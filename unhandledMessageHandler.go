package kafka

type UnhandledMessageHandler struct {
	handler MessageHandler
}

func (h *UnhandledMessageHandler) ProcessMessage(ctx *WorkerContext, message *Message) {
	if h.handler != nil {
		context := &WorkerContext{
			processUnhandledMessage: h.processRecursiveUnhandledMessage,
			consumer:                ctx.consumer,
		}
		h.handler.ProcessMessage(context, message)
	}
}

func (h *UnhandledMessageHandler) Warp(handler MessageHandler) {
	h.handler = handler
}

func (h *UnhandledMessageHandler) processRecursiveUnhandledMessage(ctx *WorkerContext, message *Message) {
	panic("invalid forward; it might be recursive forward message to unhandledMessageHandler")
}
