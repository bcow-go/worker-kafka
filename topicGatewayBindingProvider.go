package kafka

import (
	"fmt"
	"log"
	"reflect"

	"gitlab.bcowtech.de/bcow-go/host"
	proto "gitlab.bcowtech.de/bcow-go/structprototype"
	"gitlab.bcowtech.de/bcow-go/structprototype/reflectutil"
)

type topicGatewayBindingProvider struct {
	route      *Route
	appContext *host.Context
}

func (p *topicGatewayBindingProvider) BeforeBind(context *proto.PrototypeContext) error {
	return nil
}

func (p *topicGatewayBindingProvider) BindField(field proto.PrototypeField, rv reflect.Value) error {
	if !rv.IsValid() {
		return fmt.Errorf("specifiec argument 'rv' is invalid")
	}

	// assign zero if rv is nil
	rv = reflectutil.AssignZero(rv)
	err := bindMessageHandler(rv, p.appContext)
	if err != nil {
		return err
	}

	// register MessageHandlers
	if isTypeMessageHandler(rv) {
		handler := asMessageHandler(rv)
		if handler != nil {
			p.route.Add(field.Name(), handler)
		}
	}
	return nil
}

func (p *topicGatewayBindingProvider) AfterBind(context *proto.PrototypeContext) error {
	return nil
}

func bindMessageHandler(v interface{}, appContext *host.Context) error {
	// populate the ServiceProvider & Config
	provider := &messageHandlerBindingProvider{
		data: map[string]reflect.Value{
			appConfigFieldName:          appContext.FieldByName(appConfigFieldName),
			appServiceProviderFieldName: appContext.FieldByName(appServiceProviderFieldName),
		},
	}

	prototype, err := proto.Prototypify(v,
		&proto.PrototypifyConfig{
			BuildValueBinderFunc: proto.BuildNilBinder,
			StructTagResolver:    resolveMessageHandlerTag,
		})
	if err != nil {
		return err
	}

	binder, err := proto.NewPrototypeBinder(prototype, provider)
	if err != nil {
		return err
	}

	err = binder.Bind()
	if err != nil {
		return err
	}

	ctx := proto.PrototypeContext(*prototype)
	rv := ctx.Target()
	if rv.CanAddr() {
		rv = rv.Addr()
		// call MessageHandler.Init()
		fn := rv.MethodByName(componentInitMethodName)
		if fn.IsValid() {
			if fn.Kind() != reflect.Func {
				log.Fatalf("[bcow-go/worker-kafka] cannot find %s.%s() within type %[1]s\n", rv.Type().String(), componentInitMethodName)
			}
			if fn.Type().NumIn() != 0 || fn.Type().NumOut() != 0 {
				log.Fatalf("[bcow-go/worker-kafka] %s.%s() type should be func()\n", rv.Type().String(), componentInitMethodName)
			}
			fn.Call([]reflect.Value(nil))
		}
	}
	return nil
}

func resolveMessageHandlerTag(fieldname, token string) (*proto.StructTag, error) {
	tag := &proto.StructTag{
		Name: fieldname,
	}
	return tag, nil
}
