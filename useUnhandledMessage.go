package kafka

import (
	"gitlab.bcowtech.de/bcow-go/host"
)

func UseUnhandledMessage(handler MessageHandler) host.Middleware {
	if handler == nil {
		panic("argument 'handler' cannot be nil")
	}

	return &host.GenericMiddleware{
		InitFunc: func(appCtx *host.Context) {
			rvHost := appCtx.HostField()
			worker := kafkaHostProvider.asKafkaWorker(rvHost)

			err := bindMessageHandler(handler, appCtx)
			if err != nil {
				panic(err)
			}

			worker.unhandledMessageHandler.Warp(handler)
		},
	}
}
