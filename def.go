package kafka

import (
	"reflect"

	"gitlab.bcowtech.de/bcow-go/host"
)

const (
	KAFKA_CONF_BOOTSTRAP_SERVERS = "bootstrap.servers"
	KAFKA_CONF_GROUP_ID          = "group.id"

	topicGatewayTagName string = "topic"
	WildcardTopic       string = "*"

	appHostFieldName            = host.AppHostFieldName
	appConfigFieldName          = host.AppConfigFieldName
	appServiceProviderFieldName = host.AppServiceProviderFieldName
	componentInitMethodName     = host.ComponentInitMethodName
)

var (
	kafkaHostProvider = &hostProvider{}

	typeOfHost           = reflect.TypeOf(Worker{})
	typeOfMessageHandler = reflect.TypeOf((*MessageHandler)(nil)).Elem()
	typeOfTopicRewriter  = reflect.TypeOf((*TopicRewriter)(nil)).Elem()
)

type (
	TopicRewriter interface {
		RewriteTopic(input string) (topic string, ok bool)
	}
)

func isTypeMessageHandler(rv reflect.Value) bool {
	if rv.IsValid() {
		return rv.Type().AssignableTo(typeOfMessageHandler)
	}
	return false
}

func asMessageHandler(rv reflect.Value) MessageHandler {
	if rv.IsValid() {
		if v, ok := rv.Convert(typeOfMessageHandler).Interface().(MessageHandler); ok {
			return v
		}
	}
	return nil
}
