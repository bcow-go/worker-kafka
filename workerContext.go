package kafka

import (
	"time"
)

type WorkerContext struct {
	processUnhandledMessage MessageHandlerFunc
	consumer                *Consumer
}

func (c *WorkerContext) Commit() ([]TopicPartition, error) {
	return c.consumer.Commit()
}

func (c *WorkerContext) CommitMessage(m *Message) ([]TopicPartition, error) {
	return c.consumer.CommitMessage(m)
}

func (c *WorkerContext) CommitOffsets(offsets []TopicPartition) ([]TopicPartition, error) {
	return c.consumer.CommitOffsets(offsets)
}

func (c *WorkerContext) Committed(partitions []TopicPartition, timeoutMs int) (offsets []TopicPartition, err error) {
	return c.consumer.Committed(partitions, timeoutMs)
}

func (c *WorkerContext) Pause(partitions []TopicPartition) error {
	return c.consumer.Pause(partitions)
}

func (c *WorkerContext) Resume(partitions []TopicPartition) error {
	return c.consumer.Resume(partitions)
}

func (c *WorkerContext) StoreOffsets(partitions []TopicPartition) (storedOffsets []TopicPartition, err error) {
	return c.consumer.StoreOffsets(partitions)
}

func (c *WorkerContext) Wait(partitions []TopicPartition, duration time.Duration, callback func() error) error {
	var err error
	err = c.Pause(partitions)
	if err != nil {
		return err
	}

	time.AfterFunc(duration, func() {
		defer c.Resume(partitions)
		err = callback()
		if err != nil {
			return
		}
	})
	return err
}

func (c *WorkerContext) ForwardToUnhandledMessageHandler(message *Message) {
	c.processUnhandledMessage(c, message)
}
