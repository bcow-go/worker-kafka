package kafka

type MessageHandler interface {
	ProcessMessage(ctx *WorkerContext, message *Message)
}

type MessageHandlerFunc func(ctx *WorkerContext, message *Message)

func (fn MessageHandlerFunc) ProcessMessage(ctx *WorkerContext, message *Message) {
	fn(ctx, message)
}
