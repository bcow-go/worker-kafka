package test

import (
	"fmt"
	"log"
	"time"

	kafka "gitlab.bcowtech.de/bcow-go/worker-kafka"
)

type MyTopicMessageHandler struct {
	ServiceProvider *ServiceProvider
}

func (h *MyTopicMessageHandler) Init() {
	fmt.Println("MyTopicMessageHandler.Init()")
	fmt.Printf("MyTopicMessageHandler.ServiceProvider: %v\n", h.ServiceProvider)
}

func (h *MyTopicMessageHandler) ProcessMessage(ctx *kafka.WorkerContext, message *kafka.Message) {
	log.Printf("%% Message on %s:\n%s\n", message.TopicPartition, string(message.Value))
	if message.Headers != nil {
		log.Printf("%% Headers: %v\n", message.Headers)
	}
	log.Printf("%% Timestamp: %v\n", message.Timestamp)
	log.Printf("%% Timestamp: %v\n", message.Timestamp.UnixNano()/(int64(time.Millisecond)))
	log.Printf("%% TimestampType: %v\n", message.TimestampType)
	time.Sleep(time.Duration(4) * time.Second)
	if string(message.Value) == "to" {
		log.Printf("waiting 10 second...\n")
		ctx.Wait([]kafka.TopicPartition{message.TopicPartition}, 10*time.Second, func() error {
			log.Printf("afer 10 second...\n")
			return nil
		})
	}
}
