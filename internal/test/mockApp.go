package test

import (
	"fmt"
	"time"

	kafka "gitlab.bcowtech.de/bcow-go/worker-kafka"
)

type (
	MockApp struct {
		Host            *Host
		Config          *Config
		ServiceProvider *ServiceProvider
	}

	Host kafka.Worker

	Config struct {
		// kafka
		BootstrapServers string   `env:"*BOOTSTRAP_SERVERS"   yaml:"-"`
		GroupID          string   `env:"-"                    yaml:"groupID"`
		Topics           []string `env:"-"                    yaml:"topics"`
		PollingTimeoutMs int      `env:"-"                    yaml:"pollingTimeout"    arg:"polling-timeout"`
	}

	ServiceProvider struct {
		ResourceName string
	}

	MockTopicGateway struct {
		*MyTopicMessageHandler `topic:"@MY_TOPIC"`

		rewritingTable map[string]string
	}
)

func (provider *ServiceProvider) Init(conf *Config) {
	fmt.Println("ServiceProvider.Init()")
	provider.ResourceName = "demo resource"
}

func (h *Host) Init(conf *Config) {
	h.Topics = conf.Topics
	h.PollingTimeoutMs = conf.PollingTimeoutMs
	h.PingTimeout = time.Second * 3
	h.ConfigMap = &kafka.ConfigMap{
		"bootstrap.servers": conf.BootstrapServers,
		"group.id":          conf.GroupID,
		"auto.offset.reset": "earliest",
	}
	h.ErrorHandler = func(err kafka.Error) (disposed bool) {
		return true
	}
}

func (g *MockTopicGateway) Init(conf *Config) {
	fmt.Println("MockTopicGateway.Init()")
	fmt.Printf("%+v\n", conf)
	g.rewritingTable = map[string]string{
		conf.Topics[0]: "@MY_TOPIC",
	}
}

func (g *MockTopicGateway) RewriteTopic(input string) (topic string, ok bool) {
	topic, ok = g.rewritingTable[input]
	return
}
