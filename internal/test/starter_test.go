package test

import (
	"flag"
	"os"
	"reflect"
	"testing"

	"gitlab.bcowtech.de/bcow-go/config"
	"gitlab.bcowtech.de/bcow-go/host"
	kafka "gitlab.bcowtech.de/bcow-go/worker-kafka"
)

func Test(t *testing.T) {
	/* like
	$ go run app.go --polling-timeout "15"
	*/
	initializeArgs()

	app := MockApp{}
	starter := kafka.Startup(&app,
		[]host.Middleware{
			kafka.UseTopicGateway(&MockTopicGateway{}),
			kafka.UseUnhandledMessage(&UnhandledMessageHandler{}),
		}...).
		ConfigureConfiguration(func(service *config.ConfigurationService) {
			service.
				LoadEnvironmentVariables("").
				LoadYamlFile("config.yaml").
				LoadCommandArguments()
		})

	starter.Run()

	// startCtx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	// defer cancel()
	// if err := starter.Start(startCtx); err != nil {
	// 	t.Error(err)
	// }

	// stopCtx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	// defer cancel()
	// if err := starter.Stop(stopCtx); err != nil {
	// 	t.Error(err)
	// }

	// assert app.Config
	{
		conf := app.Config
		if conf.BootstrapServers == "" {
			t.Errorf("assert 'Config.BootstrapServers':: should not be an empty string")
		}
		if conf.GroupID != "DemoOutputWorker" {
			t.Errorf("assert 'Config.GroupID':: expected '%v', got '%v'", true, conf.GroupID)
		}
		expectedTopics := []string{"myTopic"}
		if !reflect.DeepEqual(conf.Topics, expectedTopics) {
			t.Errorf("assert 'Config.Topics':: expected '%#v', got '%#v'", expectedTopics, conf.Topics)
		}
		if conf.PollingTimeoutMs != 15 {
			t.Errorf("assert 'Config.PollingTimeoutMs':: expected '%v', got '%v'", 15, conf.PollingTimeoutMs)
		}
	}

}

func initializeArgs() {
	os.Args = []string{"example",
		"--polling-timeout", "15"}

	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.ExitOnError)
}
