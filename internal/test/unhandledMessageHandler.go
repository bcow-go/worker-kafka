package test

import (
	"fmt"

	kafka "gitlab.bcowtech.de/bcow-go/worker-kafka"
)

type UnhandledMessageHandler struct {
	ServiceProvider *ServiceProvider
}

func (h *UnhandledMessageHandler) Init() {
	fmt.Println("UnhandledMessageHandler.Init()")
	fmt.Printf("UnhandledMessageHandler.ServiceProvider: %v\n", h.ServiceProvider)
}

func (h *UnhandledMessageHandler) ProcessMessage(ctx *kafka.WorkerContext, message *kafka.Message) {
	fmt.Println("UnhandledMessageHandler.ProcessMessage()")
	// ctx.ForwardToUnhandledMessageHandler(message)
}
