package kafka

import (
	"fmt"
	"reflect"

	"gitlab.bcowtech.de/bcow-go/host"
	proto "gitlab.bcowtech.de/bcow-go/structprototype"
)

func UseTopicGateway(gateway interface{}) host.Middleware {
	if gateway == nil {
		panic("argument 'gateway' cannot be nil")
	}

	return &host.GenericMiddleware{
		InitFunc: func(appCtx *host.Context) {
			rvHost := appCtx.HostField()
			worker := kafkaHostProvider.asKafkaWorker(rvHost)

			route := NewRoute()
			err := bindTopicGateway(gateway, route, appCtx)
			if err != nil {
				panic(err)
			}

			var rewriter TopicRewriter = new(echoTopicRewriter)
			if v, ok := gateway.(TopicRewriter); ok {
				rewriter = v
			}

			worker.messageHandlerFunc = func(ctx *WorkerContext, message *Message) {
				topic := *message.TopicPartition.Topic

				var handler MessageHandler
				if handlerRef, ok := rewriter.RewriteTopic(topic); ok {
					handler = route.Get(handlerRef)
				}
				if handler == nil {
					handler = route.Get(WildcardTopic)
				}
				if handler != nil {
					handler.ProcessMessage(ctx, message)
					return
				}
				worker.unhandledMessageHandler.ProcessMessage(ctx, message)
			}
		},
	}
}

func bindTopicGateway(v interface{}, route *Route, appCtx *host.Context) error {
	provider := &topicGatewayBindingProvider{
		route:      route,
		appContext: appCtx,
	}

	prototype, err := proto.Prototypify(v,
		&proto.PrototypifyConfig{
			TagName:              topicGatewayTagName,
			BuildValueBinderFunc: proto.BuildNilBinder,
			StructTagResolver:    resolveTopicTag,
		})
	if err != nil {
		return err
	}

	binder, err := proto.NewPrototypeBinder(prototype, provider)
	if err != nil {
		return err
	}

	err = binder.Bind()
	if err != nil {
		return err
	}

	ctx := proto.PrototypeContext(*prototype)
	rv := ctx.Target()
	if rv.CanAddr() {
		pv := rv.Addr()
		// call Init() method on specified implement
		if pv.Type().AssignableTo(typeOfTopicRewriter) {
			var (
				pConfig = appCtx.FieldByName(appConfigFieldName)
			)

			// call TopicGateway.Init()
			fn := pv.MethodByName(componentInitMethodName)
			if !fn.IsValid() || fn.Kind() != reflect.Func ||
				fn.Type().NumIn() != 1 || fn.Type().NumOut() != 0 || fn.Type().In(0) != pConfig.Type() {
				panic(fmt.Errorf("[bcow-go/worker-kafka] should implement func %s.%s(conf %s)",
					rv.Type().String(),
					componentInitMethodName,
					pConfig.Type().String()))
			}
			fn.Call([]reflect.Value{pConfig})
		}
	}
	return nil
}

func resolveTopicTag(fieldname, token string) (*proto.StructTag, error) {
	var tag *proto.StructTag
	if token != "" && token != "-" {
		tag = &proto.StructTag{
			Name: token,
		}
	}
	return tag, nil
}
