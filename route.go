package kafka

type Route map[string]MessageHandler

func NewRoute() *Route {
	var route Route = make(map[string]MessageHandler)
	return &route
}

func (r *Route) Add(topic string, handler MessageHandler) {
	route := *r
	route[topic] = handler
}

func (r *Route) Remove(topic string) {
	route := *r
	delete(route, topic)
}

func (r *Route) Get(topic string) MessageHandler {
	route := *r
	if v, ok := route[topic]; ok {
		return v
	}
	return nil
}
