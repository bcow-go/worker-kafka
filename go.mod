module gitlab.bcowtech.de/bcow-go/worker-kafka

go 1.14

require (
	github.com/confluentinc/confluent-kafka-go v1.4.2 // indirect
	gitlab.bcowtech.de/bcow-go/config v1.2.1
	gitlab.bcowtech.de/bcow-go/host v1.10.5
	gitlab.bcowtech.de/bcow-go/structprototype v1.3.0
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.4.2
)
